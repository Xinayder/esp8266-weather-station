// Wifi SSID to connect to
#define WIFI_SSID ""
// Wifi password
#define WIFI_PASSWORD ""
// MQTT server to connect to
#define MQTT_BROKER ""
#define MQTT_PORT 1883
// MQTT username
#define MQTT_USERNAME ""
// MQTT password
#define MQTT_PASSWORD ""