#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <BME280I2C.h>
#include <BH1750.h>
#include <Wire.h>
#include <SPI.h>
#include <PubSubClient.h>
#include "station_config.h"


/*
    Pluviometro: D10 (pulldown fisico)
    Anemometro: D7 (pullup fisico)
    BME280: I2C (D3/D4)
    BH1750: I2C (D3/D4)
    3V3: D6
*/

#define PLUVIOMETRO D10
#define ANEMOMETRO D7
#define I2C_INPUT D6

char MQTT_CLIENT_ID[18] = {0};
char MQTT_TOPIC[26] = {0};

const unsigned long INTERVALO_DIA = 86400000UL;
const unsigned long INTERVALO_SEIS_HORAS = 21600000UL;
const unsigned long INTERVALO_HORA = 3600000UL;
const unsigned long INTERVALO_MINUTO = 60000UL;
const unsigned long INTERVALO_SEGUNDO = 1000UL;

const unsigned long ANEMOMETRO_TEMPO_AMOSTRA = 7500;
const unsigned long PLUVIOMETRO_TEMPO_AMOSTRA = INTERVALO_HORA;

const float RPM_TO_MPS = 50.748803;
const float RPM_TO_KMH = 14.0968897222;

WiFiEventHandler wifiAssignIPHandler;
WiFiEventHandler wifiDisconnectHandler;

BME280I2C bme;
BH1750 sensorLuz;
WiFiClient wifi_client;
PubSubClient mqtt_client(wifi_client);

// Anemometro
volatile unsigned long anemometro_pulsos;
volatile unsigned long anemometro_tempo;
volatile unsigned long anemometro_intervalo_pulsos;
volatile unsigned long anemometro_tempo_pulsos;
volatile unsigned long anemometro_tempo_pulsos_anterior;
volatile unsigned long anemometro_rpm_temp;
volatile bool anemometro_leitura_realizada;

// Anemometro v2.0
volatile unsigned long anem_tempo_inicio = 0; // tempo de inicio do calculo da velocidade
volatile float anem_tempo_pulso = 0; // tempo entre pulsos
volatile float anem_tempo_acumulado = 0; // tempo entre pulsos acumulado para calculo da velocidade media
volatile bool anem_inicio = true; // início da leitura do anemômetro
volatile unsigned int anem_media_pulsos = 0; // quantidade de pulsos para calcular a velocidade média
volatile unsigned long anem_t_deb = 0;
volatile unsigned long anem_t_deb_ant = 0;

// Pluviometro
volatile unsigned long pluviometro_tempo_pulsos;
volatile unsigned long pluviometro_tempo_pulsos_anterior;
volatile unsigned int pluviometro_pulsos_dia;

unsigned int pluviometro_pulsos_dia_hora_anterior;
unsigned int pluviometro_pulsos_dia_seis_horas_anterior;
unsigned int pluviometro_pulsos_seis_horas;
unsigned int pluviometro_pulsos_hora;

unsigned long pluviometro_tempo_hora;
unsigned long pluviometro_tempo_seis_horas;
unsigned long pluviometro_tempo_dia;


// Contagem de tempo
unsigned long tempo_anterior;
unsigned long tempo_mqtt_conexao;
unsigned long tempo_mqtt_envia;
unsigned long tempo_leitura;
unsigned long tempo_leitura_bme;

unsigned int anemometro_rpm;

struct sensor_data {
    float temp;
    float pres;
    float hum;
    float lux;
};

void onWifiAssignIP(const WiFiEventStationModeGotIP &evt) {
    Serial.print("Connected, got IP: ");
    Serial.println(WiFi.localIP());

    ArduinoOTA.begin();
   
    Serial.print("MQTT connected: ");
    Serial.print(mqtt_client.connected());
    Serial.print(" (");
    Serial.print(mqtt_client.state());
    Serial.println(")");
}

void onWifiDisconnect(const WiFiEventStationModeDisconnected &evt) {
    Serial.print("Disconnected from WiFi: ");
    Serial.println(evt.reason);
}

void wifi_setup(const char* ssid, const char* password) {
    if (WiFi.SSID() != ssid) {
        WiFi.hostname(MQTT_CLIENT_ID);
        WiFi.mode(WIFI_STA);
        WiFi.setAutoConnect(true);
        WiFi.setAutoReconnect(true);
        WiFi.persistent(true);
        WiFi.begin(ssid, password);
    }

    wifiAssignIPHandler = WiFi.onStationModeGotIP(&onWifiAssignIP);
    wifiDisconnectHandler = WiFi.onStationModeDisconnected(&onWifiDisconnect);
}

void wifi_wait_connect_reboot() {
    while (WiFi.waitForConnectResult() != WL_CONNECTED) {
        Serial.println("Connection Failed! Rebooting...");
        delay(5000);
        ESP.restart();
    }
}

void ota_setup() {

}

bool mqtt_connect() {
    mqtt_client.connect(MQTT_CLIENT_ID, MQTT_USERNAME, MQTT_PASSWORD, "teste", 0, false, MQTT_CLIENT_ID);
    return mqtt_client.connected();
}

void mqtt_setup() {
    sprintf(MQTT_TOPIC, "casa/estacao/%s/", MQTT_CLIENT_ID);
    mqtt_client.setServer(MQTT_BROKER, MQTT_PORT);
    mqtt_connect();
}

ICACHE_RAM_ATTR void pluviometro_isr() {
    pluviometro_tempo_pulsos = micros();

    if ((pluviometro_tempo_pulsos - pluviometro_tempo_pulsos_anterior) > 5000) {
        pluviometro_pulsos_dia++;
        pluviometro_tempo_pulsos_anterior = pluviometro_tempo_pulsos;
    }
}

ICACHE_RAM_ATTR void anemometro_isr() {
    //anemometro_pulsos++;

    /*anemometro_tempo_pulsos = micros();

    // 10ms de debouncing
    if ((anemometro_tempo_pulsos - anemometro_tempo_pulsos_anterior) > 10000) {
        if (anemometro_pulsos == 0)
            anemometro_intervalo_pulsos = 0;
        else
            anemometro_intervalo_pulsos += (anemometro_tempo_pulsos - anemometro_tempo_pulsos_anterior);
        
        if (anemometro_pulsos) {
            anemometro_rpm_temp = (60000000UL / anemometro_intervalo_pulsos);
            anemometro_pulsos = 0;
            anemometro_intervalo_pulsos = 0;
            anemometro_leitura_realizada = true;
        }
        anemometro_pulsos++;
        anemometro_tempo_pulsos_anterior = anemometro_tempo_pulsos;
    }*/

    anem_t_deb = millis();
    if ((anem_t_deb - anem_t_deb_ant) > 0) {

        unsigned long t_atual = millis();
        // calcula o intervalo entre pulsos se não for o primeiro pulso
        if (!anem_inicio) {
            anem_tempo_pulso = (float)(t_atual - anem_tempo_inicio) / 1000;
            anem_tempo_acumulado += anem_tempo_pulso; // soma o intervalo entre pulsos para o cálculo da média
            anem_media_pulsos++; // soma a quantidade de pulsos para fazer a média
        }

        anem_tempo_inicio = millis();
        anem_inicio = false;

        anem_t_deb_ant = anem_t_deb; // tempo de debouncing
    }
}


void bme280_setup() {
    /*while (!bme.begin()) {
        delay(1000);
    }*/
    bme.begin();

    BME280I2C::Settings _indoor_nav_settings = BME280I2C::Settings(
        BME280::OSR_X2,
        BME280::OSR_X16,
        BME280::OSR_X1,
        BME280::Mode_Normal,
        BME280::StandbyTime_500us,
        BME280::Filter_16,
        BME280::SpiEnable_False,
        BME280I2C::I2CAddr_0x76
    );

    BME280I2C::Settings _weather_settings = BME280I2C::Settings(
        BME280::OSR_X1,
        BME280::OSR_X1,
        BME280::OSR_X1,
        BME280::Mode_Forced,
        BME280::StandbyTime_1000ms,
        BME280::Filter_Off,
        BME280::SpiEnable_False,
        BME280I2C::I2CAddr_0x76
    );

    bme.setSettings(_weather_settings);
}

void bh1750_setup() {
    /*while (!sensorLuz.begin()) {
        delay(1000);
    }*/
    sensorLuz.begin(BH1750::ONE_TIME_HIGH_RES_MODE);
}

float anem_freq(float t_pulso) {
    return 1 / t_pulso;
}

float anem_vel_kmh(float freq) {
   float rpm = 60 * freq;
   return (3 * PI * 0.178 * rpm) / 25;
}

float anem_vel_media(float n_pulsos, int periodo) {
    if (periodo)
        return anem_vel_kmh(anem_freq((float)(n_pulsos / periodo)));
    return 0;
}

void setup() {
    //Serial.begin(115200);
    Serial.println();

    sprintf(MQTT_CLIENT_ID, "WEMOS_%06X", ESP.getChipId());

    wifi_setup(WIFI_SSID, WIFI_PASSWORD);
    wifi_wait_connect_reboot();

    ArduinoOTA.onStart([]() {
        String type;
        if (ArduinoOTA.getCommand() == U_FLASH) {
        type = "sketch";
        } else { // U_FS
        type = "filesystem";
        }

        // NOTE: if updating FS this would be the place to unmount FS using FS.end()
        Serial.println("Start updating " + type);
    });
    ArduinoOTA.onEnd([]() {
        Serial.println("\nEnd");
        ESP.restart();
    });
    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
        Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    });
    ArduinoOTA.onError([](ota_error_t error) {
        Serial.printf("Error[%u]: ", error);
        if (error == OTA_AUTH_ERROR) {
        Serial.println("Auth Failed");
        } else if (error == OTA_BEGIN_ERROR) {
        Serial.println("Begin Failed");
        } else if (error == OTA_CONNECT_ERROR) {
        Serial.println("Connect Failed");
        } else if (error == OTA_RECEIVE_ERROR) {
        Serial.println("Receive Failed");
        } else if (error == OTA_END_ERROR) {
        Serial.println("End Failed");
        }
    });
    ArduinoOTA.setHostname(MQTT_CLIENT_ID);
    ArduinoOTA.begin();

    pinMode(I2C_INPUT, OUTPUT);
    digitalWrite(I2C_INPUT, HIGH);

    mqtt_setup();

    Wire.begin();
    bme280_setup();
    bh1750_setup();

    pinMode(PLUVIOMETRO, INPUT);
    pinMode(ANEMOMETRO, INPUT_PULLUP);
    pinMode(LED_BUILTIN, OUTPUT);

    attachInterrupt(digitalPinToInterrupt(PLUVIOMETRO), pluviometro_isr, RISING);
    attachInterrupt(digitalPinToInterrupt(ANEMOMETRO), anemometro_isr, RISING);

    pluviometro_pulsos_hora = 0;
    pluviometro_pulsos_seis_horas = 0;
    pluviometro_pulsos_dia = 0;
    pluviometro_tempo_hora = 0;
    pluviometro_tempo_seis_horas = 0;
    pluviometro_tempo_dia = 0;
    pluviometro_tempo_pulsos = 0;
    pluviometro_tempo_pulsos_anterior = 0;

    anemometro_tempo = 0;

    anemometro_leitura_realizada = false;
    anemometro_rpm = false;
    anemometro_pulsos = 0;


    tempo_leitura = 0;
    tempo_leitura_bme = 0;
    tempo_mqtt_conexao = 0;
    tempo_mqtt_envia = 0;
}

const char* biruta_obter_direcao(int valor) {
    if (valor >= 0 && valor <  401)
        return "N";
    else if (valor >= 401 && valor < 551)
        return "NE";
    else if (valor >= 551 && valor < 651)
        return "E";
    else if (valor >= 651 && valor < 751)
        return "SE";
    else if (valor >= 751 && valor < 831)
        return "S";
    else if (valor >= 831 && valor < 891)
        return "SO";
    else if (valor >= 891 && valor < 941)
        return "O";
    else if (valor >= 941 && valor <= 1023)
        return "NO";
}

sensor_data sensores_obter_dados() {
    static float temp(0), hum(0), pres(0), lux(0);

    BME280::TempUnit temp_unit(BME280::TempUnit_Celsius);
    BME280::PresUnit pres_unit(BME280::PresUnit_Pa);

    if (tempo_leitura_bme == 0 || millis() - tempo_leitura_bme >= INTERVALO_MINUTO) {
        tempo_leitura_bme = millis();
        bme.read(pres, temp, hum, temp_unit, pres_unit);
    }

    lux = sensorLuz.readLightLevel(true);

    if (lux < 0.0 || temp == NAN) {
        digitalWrite(I2C_INPUT, LOW);
        delay(50);
        digitalWrite(I2C_INPUT, HIGH);
        delay(50);

        bme280_setup();
        bh1750_setup();

        delay(50);
    }

    // Ajusta o tempo de leitura do sensor de luz
    // conforme a luminosidade
    if (lux > 40000.0)
        sensorLuz.setMTreg(32);
    else if (lux > 10.0)
        sensorLuz.setMTreg(69);
    else if (lux <= 10.0)
        sensorLuz.setMTreg(138);

    return sensor_data{
        temp = temp,
        pres = pres,
        hum = hum,
        lux = lux
    };
}

// the loop function runs over and over again forever
void loop() {
    ArduinoOTA.handle();

    // Somente executa o loop do MQTT se o cliente estiver conectado
    if (mqtt_client.connected()) {
        mqtt_client.loop();
    }

    static unsigned long tempo_atual, _anemometro_tempo_reset;

    Serial.print("MQTT: ");
    Serial.println(mqtt_client.state());

    static sensor_data dados_sensores = sensores_obter_dados();
    static String mqtt_topic = String(MQTT_TOPIC);

    unsigned int _pluviometro_pulsos;

    /*unsigned long _anemometro_intervalo_pulsos = 0;
    unsigned long _anemometro_intervalo_ultimo_pulso = 0;*/

    const char* _vento_direcao;

    float temp(0), hum(0), pres(0), lux(0);
    temp = dados_sensores.temp;
    hum = dados_sensores.hum;
    pres = dados_sensores.pres;
    lux = dados_sensores.lux;

    /*BME280::TempUnit temp_unit(BME280::TempUnit_Celsius);
    BME280::PresUnit pres_unit(BME280::PresUnit_Pa);*/

    //lux = sensorLuz.readLightLevel(true);

    //bme.read(pres, temp, hum, temp_unit, pres_unit);

    float _vel_vento_mps(0), _vel_vento_kmh(0);

    tempo_atual = millis();

    if (tempo_atual - tempo_leitura >= INTERVALO_SEGUNDO * 5) {
        tempo_leitura = millis();

        dados_sensores = sensores_obter_dados();
    }

    // Reseta a contagem do anemômetro depois de 2,5s
    if ((tempo_atual - anem_tempo_inicio) >= 2500) {
        anem_tempo_pulso = 0;
    }

    // Faz os calculos a cada segundo
    if (tempo_atual - tempo_anterior >= INTERVALO_SEGUNDO) {
        tempo_anterior = millis();

        // Calculo do anemometro
        noInterrupts();
        _vel_vento_kmh = anem_vel_media(anem_tempo_acumulado, anem_media_pulsos); // calcula a velocidade média do vento
        anem_tempo_acumulado = 0; // reseta o intervalo acumulado
        anem_media_pulsos = 0; // reseta a contagem acumulada

        float a_freq = 0;
        if (anem_tempo_pulso) a_freq = anem_freq(anem_tempo_pulso);
        //_vel_vento_kmh = anem_vel_kmh(a_freq);

        anem_inicio = true;
        interrupts();

        //noInterrupts();

        // Pluviometro
        /*if (pluviometro_tempo_pulsos_anterior && pluviometro_tempo_pulsos) {
            _pluviometro_tempo_pulsos = (pluviometro_tempo_pulsos - pluviometro_tempo_pulsos_anterior);
        }*/

        // Se o intervalo entre pulsos for maior que o intervalo de 1mm
        /*if (tempo_atual - pluviometro_tempo_pulsos > (PLUVIOMETRO_TEMPO_AMOSTRA / 4))
            _pluviometro_tempo_pulsos = 0;*/

        //if (pluviometro_tempo_pulsos)
        //    _pluviometro_tempo_ultimo_pulso = tempo_atual - pluviometro_tempo_pulsos;



        // Anemometro
        /*if (anemometro_tempo_pulsos_anterior && anemometro_tempo_pulsos) {
            _anemometro_intervalo_pulsos = anemometro_tempo_pulsos - anemometro_tempo_pulsos_anterior;
        }

        if (tempo_atual - anemometro_tempo_pulsos > INTERVALO_SEGUNDO * 10) {
            _anemometro_intervalo_pulsos = 0;
            _vel_vento_mps = 0;
            _vel_vento_kmh = 0;
        }*/

        //if (anemometro_tempo_pulsos)
        //    _anemometro_intervalo_ultimo_pulso = tempo_atual - anemometro_tempo_pulsos;
        
        //interrupts();

        noInterrupts();
        _pluviometro_pulsos = pluviometro_pulsos_dia;
        interrupts();

        pluviometro_pulsos_hora = _pluviometro_pulsos - pluviometro_pulsos_dia_hora_anterior;

        pluviometro_pulsos_seis_horas = _pluviometro_pulsos - pluviometro_pulsos_dia_seis_horas_anterior;

        pluviometro_pulsos_dia_hora_anterior = _pluviometro_pulsos;
        pluviometro_pulsos_dia_seis_horas_anterior = _pluviometro_pulsos;

        _vento_direcao = biruta_obter_direcao(analogRead(A0));

        // anemometro antigo
        // Calcula a velocidade do vento
        /*if (anemometro_rpm) {
            //_vel_vento_mps = (1000 / _anemometro_intervalo_ultimo_pulso) * 0.08 * 2 * 3.14;
            // m/s = (RPM / 60) * 2pi * R

            //_vel_vento_mps = (anemometro_rpm / 60) * 2 * 3.14 * 0.08;
            _vel_vento_mps = anemometro_rpm / RPM_TO_MPS;
            _vel_vento_kmh = anemometro_rpm / RPM_TO_KMH;
        }*/

        // Calcula a quantidade de chuva acumulada na ultima hora
        //if (_pluviometro_tempo_ultimo_pulso && _pluviometro_tempo_ultimo_pulso < PLUVIOMETRO_TEMPO_AMOSTRA / 4)
        //    _pluviometro_chuva_hora = PLUVIOMETRO_TEMPO_AMOSTRA / (_pluviometro_tempo_ultimo_pulso * 4);
    }

    // anemometro
    // Interrupcao do anemometro aconteceu
    /*if (anemometro_leitura_realizada) {
        anemometro_leitura_realizada = false;
        noInterrupts();
        anemometro_rpm = anemometro_rpm_temp;
        interrupts();
        _anemometro_tempo_reset = millis() + (10 * INTERVALO_SEGUNDO);
    }

    // Reseta a contagem de RPM do anemometro se nao obtivermos uma leitura
    // no intervalo especificado
    if (millis() > _anemometro_tempo_reset) {
        anemometro_rpm = 0;
        anemometro_pulsos = 0;
    }*/

    // Conta a quantidade de chuva na ultima hora
    if (tempo_atual - pluviometro_tempo_hora >= INTERVALO_HORA) {
        pluviometro_tempo_hora = millis();

        pluviometro_pulsos_hora = 0;
    }

    // Conta a quantidade de chuva nas ultimas seis horas
    if (tempo_atual - pluviometro_tempo_seis_horas >= INTERVALO_SEIS_HORAS) {
        pluviometro_tempo_seis_horas = millis();

        pluviometro_pulsos_seis_horas = 0;
    }

    // Conta a quantidade de chuva no ultimo dia
    if (tempo_atual - pluviometro_tempo_dia >= INTERVALO_DIA) {
        pluviometro_tempo_dia = millis();

        noInterrupts();
        pluviometro_pulsos_dia = 0;
        interrupts();
        pluviometro_pulsos_dia_seis_horas_anterior = 0;
        pluviometro_pulsos_dia_hora_anterior = 0;
    }

    // Tenta reconectar ao servidor MQTT a cada 5 segundos
    if (!mqtt_client.connected()) {
        if (tempo_atual - tempo_mqtt_conexao >= INTERVALO_SEGUNDO * 5) {
            tempo_mqtt_conexao = millis();

            if (mqtt_connect())
                tempo_mqtt_conexao = 0;
        }
    }

    if (tempo_atual - tempo_mqtt_envia >= 5000) {
        tempo_mqtt_envia = millis();

        noInterrupts();

        char json_payload[192] = {0};
        sprintf(json_payload, "{\"T\":%.2f,\"P\":%.2f,\"H\":%.2f,\"L\":%.2f,\"WS\":%.2f,\"WD\":\"%s\",\"RI1\":%d,\"RI6\":%d,\"RI24\":%d,\"TI\":%lu,\"RPM\":%.2f}",
            temp, pres, hum, lux, _vel_vento_kmh, _vento_direcao, pluviometro_pulsos_hora, pluviometro_pulsos_seis_horas, pluviometro_pulsos_dia, tempo_atual, anem_media_pulsos);

        interrupts();
        
        /*mqtt_client.publish(String(mqtt_topic + "temperature").c_str(), String(temp).c_str());
        mqtt_client.publish(String(mqtt_topic + "pressure").c_str(), String(pres).c_str());
        mqtt_client.publish(String(mqtt_topic + "humidity").c_str(), String(hum).c_str());
        mqtt_client.publish(String(mqtt_topic + "wind_speed").c_str(), String(_vel_vento_kmh).c_str());
        mqtt_client.publish(String(mqtt_topic + "rain_intensity_1h").c_str(), String(pluviometro_pulsos_hora).c_str());
        mqtt_client.publish(String(mqtt_topic + "light_intensity").c_str(), String(lux).c_str()); 
        mqtt_client.publish(String(mqtt_topic + "wind_direction").c_str(), _vento_direcao);
        mqtt_client.publish(String(mqtt_topic + "rain_intensity_6h").c_str(), String(pluviometro_pulsos_seis_horas).c_str());
        mqtt_client.publish(String(mqtt_topic + "rain_intensity_24h").c_str(), String(pluviometro_pulsos_dia).c_str());*/
        mqtt_client.publish(String(mqtt_topic + "sensor_data").c_str(), json_payload);

        //mqtt_client.publish(String(mqtt_topic).c_str(), "hey");
    }

    delay(250);
}
